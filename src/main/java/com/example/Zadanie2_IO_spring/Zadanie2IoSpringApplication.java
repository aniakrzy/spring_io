package com.example.Zadanie2_IO_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Zadanie2IoSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(Zadanie2IoSpringApplication.class, args);
		System.out.println("Hello, World!");
	}

}
